package com.wzb.provider.service.impl;

import com.wzb.service.TestService;
import org.apache.dubbo.config.annotation.Service;
@Service(version = "1.0.0")
public class TestServiceImpl implements TestService {
    @Override
    public void ins() {
        try{
            Thread.sleep(5000);
        }catch (Exception e){
            System.out.println("异常");
        }

        System.out.println("insert");
    }

    @Override
    public void del() {
        System.out.println("delete");
    }

    @Override
    public void upd() {
        System.out.println("update");
    }

    @Override
    public void sel() {
        System.out.println("select");
    }
}
