package com.wzb.service;

/**
 *
 * 一些模拟数据库事务的方法
 */
public interface TestService {
    public void ins();
    public void del();
    public void upd();
    public void sel();
}
